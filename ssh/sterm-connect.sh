#!/bin/bash
set -e
config=~/.ssh/sterm-hosts
touch $config
menu(){
    echo -ne "\033c"
    num=0
    select selection in "Add New host" "Edit config file" $(list_hosts) ; do
        num=$REPLY
        break
    done
    if [[ "$num" == 2 ]] ; then
        if [[ ! -n $EDITOR ]] ; then
            export EDITOR="busybox vi"
        fi
        exec $EDITOR "$config"
    elif [[ "$num" == 1 ]] ; then
        echo -ne "\033[33;1mServer Name >>> \033[;0m"
        read name
        echo -ne "\033[33;1mPort        >>> \033[;0m"
        read port
        echo -ne "\033[33;1mHost        >>> \033[;0m"
        read host
        echo -ne "\033[33;1mUser        >>> \033[;0m"
        read user
        echo "[$name]" >> $config
        echo "port=$port" >> $config
        echo "host=$host" >> $config
        echo "user=$user" >> $config
        echo "key=" >> $config
        echo  >> $config
    else
        port=$(get_value $selection "port")
        host=$(get_value $selection "host")
        user=$(get_value $selection "user")
        key=$(get_value $selection "key")
        echo -en "\033c"
        if [[ -f ~/.ssh/id_rsa.pub ]] ; then
            ssh-copy-id -p "$port" "$user@$host" ${key:+-i $key}
        fi
        exec ssh -X -Y -p "$port" "$user@$host" ${key:+-i $key} $@
    fi
}

list_hosts(){
    cat $config | grep -e "^\[.*\]$" | sed "s/^.//;s/.$//;s/ /-/g"
}

get_value(){
    e=false
    cat $config | sed "s/ /-/g" | while read line ; do
        if [[ "[$1]" == "$line" ]] ; then
            e=true
        elif [[ "$e" == true ]] ; then
            if echo $line | grep -e "^$2=.*" >/dev/null ; then
                echo "$line" | cut -f 2 -d "="
                return
            fi
        fi
    done
}

while menu ; do true ; done
