sterm
=====
Sulincix terminal

Installation:
^^^^^^^^^^^^^
.. code-block:: shell

	make
	make install DESTDIR=/

Dependencies:
^^^^^^^^^^^^^
* vte
* gtk
* ssh (for sterm-connect)

Usage:
^^^^^^
.. code-block:: shell

	Usage: sterm [--daemon/--tray/--no-header] [--bare]
	--daemon    :	 Daemon mode. create trayicon but don't open window.
	--tray      :	 Create tray icon.
	--bare      :	 Pure VTE mode. Doesn't have top bar.
	--no-header :	 No header. Get old classic layout.
	--ssh       :	 Start new ssh connection.
	--help      :	 Show this message
	
	Hotkeys:
	 ctrl + shift + a   :	 Select all
	 ctrl + shift + n   :	 New tab
	 ctrl + shift + s   :	 New ssh connection
	 ctrl + shift + m   :	 New window
	 ctrl + shift + r   :	 Reset
	 ctrl + shift + <-  :	 Previous tab
	 ctrl + shift + ->  :	 Next tab
	 ctrl + shift + del :	 Delete tab
	 ctrl + shift + v   :	 Paste
	 ctrl + shift + c   :	 Copy
	 ctrl + shift + -   :	 Zoom out
	 ctrl + shift + +   :	 Zoom in
	 ctrl + shift + o   :	 Zoom %100
	 f11                :	 Fullscreen/Leave Fullscreen
	 f2                 :	 Rename window


Changing colors:
^^^^^^^^^^^^^^^^

.. code-block:: shell

	# example background - foreground
	gsettings set org.sulin.sterm bgr '#000000'
	gsettings set org.sulin.sterm fgr '#ffffff'
	# example color palette
	gsettings set org.sulin.sterm black '#000000'
	gsettings set org.sulin.sterm green '#00ff00'
	gsettings set org.sulin.sterm yellow '#ffff00'
	gsettings set org.sulin.sterm blue '#0000ff'
	gsettings set org.sulin.sterm magenta '#ff00ff'
	gsettings set org.sulin.sterm cyan '#00ffff'
	gsettings set org.sulin.sterm white '#ffffff'
	# example bright color palette
	gsettings set org.sulin.sterm bright-black '#333333'
	gsettings set org.sulin.sterm bright-green '#33ee33'
	gsettings set org.sulin.sterm bright-yellow '#eeee33'
	gsettings set org.sulin.sterm bright-blue '#3333ee'
	gsettings set org.sulin.sterm bright-magenta '#ee33ee'
	gsettings set org.sulin.sterm bright-cyan '#33eeee'
	gsettings set org.sulin.sterm bright-white '#eeeeee'


