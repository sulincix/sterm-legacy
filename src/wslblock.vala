// --pkg gio-2.0 --pkg posix needed
void wsl_block(){
    try{
        var file = File.new_for_path ("/proc/version");
        var dis = new DataInputStream (file.read ());
        string line="";
        while ((line = dis.read_line()) != null) {
            if (line != null){
                line = line.down();
            }
            if("microsoft" in line || "wsl" in line){
                Process.exit (1);
            }
        }
        var file2 = File.new_for_path ("/proc/version");
        var dis2 = new DataInputStream (file2.read ());
        while ((line = dis2.read_line()) != null) {
            if (line != null){
                line = line.down();
            }
            if("microcode" in line || "0xffffffff" in line){
                Process.exit (1);
            }
        }
    }catch{
        Process.exit (1);
    }
            
}
