using Gtk;
using Vte;
using Posix;

public int pid=0;
public string sterm_name;
public string[] arguments;
public Window App;
public int total_win=0;
int main(string[] args){
    if("--help" in args || "-h" in args){
        Posix.stdout.printf("\n");
        Posix.stdout.printf("\033[35;1mUsage:\033[;0m "+args[0]+" [--daemon/--tray/--no-header] [--bare]\n");
        Posix.stdout.printf("\033[34;1m--daemon    \033[;0m:\t Daemon mode. create trayicon but don't open window.\n");
        Posix.stdout.printf("\033[34;1m--tray      \033[;0m:\t Create tray icon.\n");
        Posix.stdout.printf("\033[34;1m--bare      \033[;0m:\t Pure VTE mode. Doesn't have top bar.\n");
        Posix.stdout.printf("\033[34;1m--no-header \033[;0m:\t No header. Get old classic layout.\n");
        Posix.stdout.printf("\033[34;1m--ssh       \033[;0m:\t Start new ssh connection.\n");
        Posix.stdout.printf("\033[34;1m--help      \033[;0m:\t Show this message\n");
        Posix.stdout.printf("\n");
        Posix.stdout.printf("\033[35;1mHotkeys:\033[;0m\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + a   \033[;0m:\t Select all\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + n   \033[;0m:\t New tab\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + s   \033[;0m:\t New ssh connection\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + m   \033[;0m:\t New window\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + r   \033[;0m:\t Reset\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + <-  \033[;0m:\t Previous tab\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + ->  \033[;0m:\t Next tab\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + del \033[;0m:\t Delete tab\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + v   \033[;0m:\t Paste\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + c   \033[;0m:\t Copy\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + -   \033[;0m:\t Zoom out\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + +   \033[;0m:\t Zoom in\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + o   \033[;0m:\t Zoom %100\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + up  \033[;0m:\t Increase opacity\n");
        Posix.stdout.printf("\033[33;1m ctrl + shift + down\033[;0m:\t Decrease opacity\n");
        Posix.stdout.printf("\033[33;1m f11                \033[;0m:\t Fullscreen/Leave Fullscreen\n");
        Posix.stdout.printf("\n");
        return 0;
    }
    wsl_block();
    arguments=args;
    pid=(int)Posix.getpid();
    sterm_name="sterm - "+(pid).to_string();
    Gtk.init(ref args);
    if(("--daemon" in args)){
        var p = fork();
        if (p != 0){
            return 0;
        }
    }
    if("--tray" in args){
        App = new AppStatusIcon(args,sterm_name);
    }else if("--headless" in args){
        var bareVTE=new Vte.Terminal();
        var w = new Gtk.Window();
        bareVTE.child_exited.connect(()=>{Gtk.main_quit();});
        try{
            GLib.Pid child_pid;
            bareVTE.spawn_sync (Vte.PtyFlags.DEFAULT,null,new string[] { args[args.length-1] },null,0,null,out child_pid,null);
            int fd = bareVTE.get_pty().get_fd();
            char buf[1024];
            Posix.readlink("/proc/"+child_pid.to_string()+"/fd/0",buf);
            printf("%s\n",(string)buf);
        }catch{
            printf("Failed to open terminal\n");
        }
        w.add(bareVTE);
    }else{
        sterm s = new sterm();
        s.sterm_init(args,sterm_name);
    }
    Gtk.Settings.get_default().set("gtk-application-prefer-dark-theme", true);
    Gtk.main();
    return 0;
}

public class scrolled : ScrolledWindow{
    public Widget vte=null;
    public override void add (Widget w){
        vte=w;
        base.add(vte);
    }
}


public class sterm : Gtk.Window{
    public Notebook n;
    public Window w;
    public Gtk.Menu Wmenu;
    public GLib.Settings settings;
    public bool isfullscreen=false;
    public Vte.Terminal bareVTE;
    
    public Gtk.Window sterm_init(string[] args,string WindowTitle){
    w = new Gtk.Window();
    settings = new GLib.Settings("org.sulin.sterm");
        w.window_position = WindowPosition.CENTER;
        set_theme(settings.get_string("bgr"),settings.get_string("fgr"));
        total_win+=1;
        w.destroy.connect(()=>{
            total_win-=1;
            if (total_win == 0 && ("--tray" in args)==false){
                Gtk.main_quit();
            }
        });
        var image = new Image();
        image.set_from_icon_name("sterm",Gtk.IconSize.MENU);
        w.icon = image.get_pixbuf();
        for(int i=0;i<args.length;i++){
            if(args[i] == "-e"){
            bareVTE=getVTE();
            if ("--wait" in args){
                bareVTE.child_exited.connect(()=>{});
            } else {
                bareVTE.child_exited.connect(()=>{Gtk.main_quit();});
            }
            var scrollbar = new scrolled();
            w.add(scrollbar);
            scrollbar.add(bareVTE);
            w.show_all();
            spawn(bareVTE,args[i+1]);
            w.resize(731,431);
            return w;
            }
        }
        if("--bare" in args){
            bareVTE=getVTE();
            var scrollbar = new scrolled();
            w.add(scrollbar);
            scrollbar.add(bareVTE);
            bareVTE.child_exited.connect(()=>{Gtk.main_quit();});
            if ("--ssh" in args){
                spawn(bareVTE,"/usr/bin/sterm-connect");
            }
        }else{
            var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 0);
            var menubar = new Gtk.MenuButton();
            n=getNotebook(args);
            w.add(box);

            menubar.set_image (new Gtk.Image.from_icon_name ("open-menu-symbolic", Gtk.IconSize.MENU));
            menubar.set_popup(Hmenu());
            menubar.set_focus_on_click(false);
            menubar.relief = ReliefStyle.NONE;
            menubar.show_all();
            
            if("--no-header" in args){
                n.set_action_widget(addNew(),PackType.START);
                var box2 = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 0);
                box2.pack_end(menubar);
                box2.pack_end(addNewWin());
                box2.pack_end(addSshNew());
                n.set_action_widget(box2,PackType.END);
                box2.show_all();
            }else{
                var box2 = new Gtk.HeaderBar ();
                box2.pack_start(addNew());
                box2.pack_start(addSshNew());
                box2.pack_end(menubar);
                box2.pack_end(addNewWin());
                box2.set_show_close_button(true);
                w.set_titlebar (box2);
            }

            box.pack_end (n);
            w.set_title(WindowTitle);
            menu_init(arguments);
            box.button_press_event.connect(display_menu);
            w.key_press_event.connect(hotkeys);
            if ("--ssh" in args){
                spawn(getCurrentVTE(arguments),"/usr/bin/sterm-connect");
            }
        }
        w.set_opacity((float)settings.get_int("window-opacity")/100);
        w.resize(731,431);
        w.show_all();
        return w;
    }
    
    public void set_theme(string bg, string fg){
        Gtk.CssProvider css_provider = new Gtk.CssProvider ();
        try{
            css_provider.load_from_data("
                * {
                    border: none;
                    box-shadow: none;
                }
                window, headerbar, headerbar *, notebook tabs, menu, notebook menu{
                    background: "+bg+";
                    color: "+fg+";
                }
                notebook *, notebook tabs* {
                    /*background: none;*/
                    border-width: 1px;
                    color: "+fg+";
                }
                 notebook tabs, menu *, dialog {
                    background: "+bg+";
                    color: "+fg+";
                }
                notebook tabs *:checked, notebook tabs:checked{
                    border-width: 1px;
                    border-radius: 6px;
                    background: none;
                    border-color: "+fg+";
                    border-style: groove;
                }
            ");
        }catch{
            print("Failed to set css");
        }
        Gtk.StyleContext.add_provider_for_screen (
                Gdk.Screen.get_default (),
                css_provider,
                Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
            );
    }
    
    public Gtk.ImageMenuItem menufull;
    public void menu_init(string[] args){
        Wmenu = new Gtk.Menu();
        if(("--bare" in args)== false){
            var menuAdd = new ImageMenuItem();
            menuAdd.set_always_show_image(true);
            menuAdd.set_image(new Gtk.Image.from_icon_name ("tab-new-symbolic", Gtk.IconSize.MENU));
            menuAdd.set_label("New Tab");
            menuAdd.activate.connect(()=>{
                create_notebook_child_label ("sterm");
            });
            Wmenu.append(menuAdd);
            var menuSshAdd = new ImageMenuItem();
            menuSshAdd.set_always_show_image(true);
            menuSshAdd.set_image(new Gtk.Image.from_icon_name ("network-server-symbolic", Gtk.IconSize.MENU));
            menuSshAdd.set_label("New Ssh Connection");
            menuSshAdd.activate.connect(()=>{
                create_notebook_child_label ("sterm");
                spawn(getCurrentVTE(arguments),"/usr/bin/sterm-connect");
            });
            Wmenu.append(menuSshAdd);
        }
        var menuAddW = new ImageMenuItem();
        menuAddW.set_always_show_image(true);
        menuAddW.set_image(new Gtk.Image.from_icon_name ("window-new-symbolic", Gtk.IconSize.MENU));
        menuAddW.set_label("New Window");
        menuAddW.activate.connect(()=>{
            sterm s = new sterm();
            s.sterm_init(arguments,sterm_name);
        });
        Wmenu.append(menuAddW);
        
        menufull = new ImageMenuItem();
        menufull.set_always_show_image(true);
        menufull.set_label("Fullscreen");
        menufull.set_image(new Gtk.Image.from_icon_name ("view-fullscreen-symbolic", Gtk.IconSize.MENU));
        menufull.activate.connect(()=>{
            if(!isfullscreen){
                isfullscreen=true;
                w.fullscreen();
            }else{
                isfullscreen=false;
                w.unfullscreen();
            }
        });
        Wmenu.append(menufull);
        
        var menucopy = new ImageMenuItem();
        menucopy.set_always_show_image(true);
        menucopy.set_label("Copy");
        menucopy.set_image(new Gtk.Image.from_icon_name ("edit-copy-symbolic", Gtk.IconSize.MENU));
        menucopy.activate.connect(()=>{
            getCurrentVTE(arguments).copy_clipboard();
        });
        Wmenu.append(menucopy);
        
        var menupaste = new ImageMenuItem();
        menupaste.set_always_show_image(true);
        menupaste.set_label("Paste");
        menupaste.set_image(new Gtk.Image.from_icon_name ("edit-paste-symbolic", Gtk.IconSize.MENU));
        menupaste.activate.connect(()=>{
            getCurrentVTE(arguments).paste_primary();
        });
        Wmenu.append(menupaste);
        
        var menuSelectAll = new ImageMenuItem();
        menuSelectAll.set_always_show_image(true);
        menuSelectAll.set_label("Select All");
        menuSelectAll.set_image(new Gtk.Image.from_icon_name ("edit-select-all-symbolic", Gtk.IconSize.MENU));
        menuSelectAll.activate.connect(()=>{
            getCurrentVTE(arguments).select_all();
        });
        Wmenu.append(menuSelectAll);

        Wmenu.show_all();
    }
    
    private bool hotkeys(Gdk.EventKey event){
        int keycode=(int)event.keyval;
        int raw_keycode=event.hardware_keycode;
        //printf("pressed : modify: %d\tcode: %d\traw_code: %d \n",event.state,keycode,raw_keycode); //dünyadaki en iyi debug yöntemi :D
        if(event.state == 21 || event.state == 37 || event.state == 36 || event.state == 5 ){ //ctrl+shift
            if(keycode == 86){ // v
                    getCurrentVTE(arguments).paste_primary();
            }else if(keycode == 67){ // c
                    getCurrentVTE(arguments).copy_clipboard();
            }else if(keycode == 65){ // a
                    getCurrentVTE(arguments).select_all();
            }else if(raw_keycode == 119){ // del
                    n.remove_page(n.get_current_page());
            }else if(raw_keycode == 114){ // ->
                    n.next_page();
            }else if(raw_keycode == 113){ // <-
                    n.prev_page();
            }else if(raw_keycode == 111){ // up
                    int cur = settings.get_int("window-opacity")+5;
                    if (cur >= 95){
                        cur = 95;
                    }
                    w.set_opacity((float)cur/100);
                    settings.set_int("window-opacity",cur);
            }else if(raw_keycode == 116){ // down
                    int cur = settings.get_int("window-opacity")-5;
                    w.set_opacity((float)cur/100);
                    if (cur <= 40){
                        cur = 40;
                    }
                    settings.set_int("window-opacity",cur);
            }else if(keycode == 78){ // n
                    create_notebook_child_label("sterm");
            }else if(keycode == 77){ // m
                    sterm s = new sterm();
                    s.sterm_init(arguments,sterm_name);
            }else if(keycode == 82){ // r
                    getCurrentVTE(arguments).reset(false,true);
                    var command = GLib.Environment.get_variable ("SHELL");
                    spawn(getCurrentVTE(arguments),command);
            }else if(keycode == 83){ // s
                    create_notebook_child_label("sterm");
                    spawn(getCurrentVTE(arguments),"/usr/bin/sterm-connect");
            }else if(raw_keycode == 86){ // +
                    for(var i=0;i<n.get_n_pages();i++){
                        var vte=(Vte.Terminal)((scrolled)n.get_nth_page(i)).vte;
                        vte.set_font_scale(getCurrentVTE(arguments).get_font_scale()+0.1);
                    }
                    settings.set_int("font-scale",(int)(getCurrentVTE(arguments).get_font_scale()*100));
            }else if(raw_keycode == 82){ // -
                    for(var i=0;i<n.get_n_pages();i++){
                        var vte=(Vte.Terminal)((scrolled)n.get_nth_page(i)).vte;
                        vte.set_font_scale(getCurrentVTE(arguments).get_font_scale()-0.1);
                    }
                    settings.set_int("font-scale",(int)(getCurrentVTE(arguments).get_font_scale()*100));
            }else if(keycode == 79){ // o
                    for(var i=0;i<n.get_n_pages();i++){
                        var vte=(Vte.Terminal)((scrolled)n.get_nth_page(i)).vte;
                        vte.set_font_scale(1.1);
                    }
                    settings.set_int("font-scale",100);
            }else{
                    return false;
            }


            return true;
        }else if(event.state == 16 || event.state == 32 || event.state == 34 || event.state == 0){
            if (raw_keycode == 95){
                if(!isfullscreen){
                    isfullscreen=true;
                    w.fullscreen();
                }else{
                    isfullscreen=false;
                    w.unfullscreen();
                }
                return true;                 
            }
        }
        return false;
    }
    
    public void spawn(Vte.Terminal vte, string command){
        try{
            vte.spawn_sync (Vte.PtyFlags.DEFAULT,null,new string[] { command },null,0,null,null);
        }catch{
            print("Failed to open terminal");
        }
    }
    
    private bool display_menu(Gdk.EventButton event){
        if(event.button == 3){//sağ tık menüsü
            if(isfullscreen){
                menufull.set_image(new Gtk.Image.from_icon_name ("view-restore-symbolic", Gtk.IconSize.MENU));
                menufull.set_label("Leave Fullscreen");
            }else{
                menufull.set_image(new Gtk.Image.from_icon_name ("view-fullscreen-symbolic", Gtk.IconSize.MENU));
                menufull.set_label("Fullscreen");
            }
            Wmenu.popup(null, null, null, event.button, event.time);
            return true;
        }
        return false;
    }
    public Vte.Terminal getCurrentVTE(string[] args){
        Vte.Terminal vte;
        if(("--bare" in args)== false){
            vte=(Vte.Terminal)((scrolled)n.get_nth_page(n.get_current_page())).vte;
        }else{
            vte=bareVTE;
        }
        return vte;
    }
    public Gtk.Notebook getNotebook(string[] args){
        n = new Gtk.Notebook();
        n.set_scrollable(true);
        n.key_press_event.connect(hotkeys);
        if(!("--no-header" in args)){
            n.set_show_tabs(false);
        }
        create_notebook_child_label("sterm");
        n.page_added.connect((w,num)=>{
            n.show_all();
            n.set_current_page((int)num);
            n.set_show_tabs(true);
        });
        n.show_all();
        n.set_current_page(1);
        n.page_removed.connect( (o, page, num) => {
            if(n.get_n_pages() == 1 && !("--no-header" in args)){
                n.set_show_tabs(false);
            }else if(n.get_n_pages() == 0 ){
                w.destroy();
            }
        });
        return n;
    }
    public Vte.Terminal getVTE(){
        var v = new Vte.Terminal();
        v.key_press_event.connect(hotkeys);
        var command = GLib.Environment.get_variable ("SHELL");
        spawn(v,command);
        v.set_scrollback_lines(-1);
        v.set_font_scale(((float)settings.get_int("font-scale"))/100);
        Gdk.RGBA[] palette = {
        // normal
        color(settings.get_string("black")),
        color(settings.get_string("red")),
        color(settings.get_string("green")),
        color(settings.get_string("yellow")),
        color(settings.get_string("blue")),
        color(settings.get_string("magenta")),
        color(settings.get_string("cyan")),
        color(settings.get_string("white")),
        // bright
        color(settings.get_string("bright-black")),
        color(settings.get_string("bright-red")),
        color(settings.get_string("bright-green")),
        color(settings.get_string("bright-yellow")),
        color(settings.get_string("bright-blue")),
        color(settings.get_string("bright-magenta")),
        color(settings.get_string("bright-cyan")),
        color(settings.get_string("bright-white")),
        };
        v.set_colors(color(settings.get_string("bgr")),color(settings.get_string("fgr")),palette);
        v.set_color_background(color(settings.get_string("bgr")));
        v.set_color_foreground(color(settings.get_string("fgr")));
        return v;
    }
    
    public Gdk.RGBA color(string c){
        var rgba = Gdk.RGBA();
        rgba.parse(c);
        return rgba;
    }
    public string hex(Gdk.RGBA color){
        string s ="#%02x%02x%02x"
        .printf((uint)(255*color.red),
        (uint)(255*color.green),
        (uint)(255*color.blue));
        return s;
    }
    public Widget create_notebook_child_label(string text){
        var vte=getVTE();
        var scrollbar = new scrolled();
        scrollbar.key_press_event.connect(hotkeys);
        var label = new Gtk.Label(text);
        var image = new Gtk.Image.from_icon_name ("list-remove-symbolic", Gtk.IconSize.MENU);
        var button = new Gtk.Button();
        
        button.relief = ReliefStyle.NONE;
        button.name = "sample-close-tab-button";
        button.clicked.connect(()=>{
            n.remove_page(n.page_num(scrollbar));
        });
        vte.window_title_changed.connect( (term) => {
            var TabTitle=vte.get_window_title();
            if(n.get_current_page()==n.page_num(vte.get_parent())){
                w.set_title(TabTitle);
            }
            if(TabTitle.length>23){
                TabTitle=TabTitle.substring(0, 20)+"...";
            }else if (TabTitle.length<5){
                TabTitle="sterm";
            }
            label.set_label(TabTitle);
        });
        vte.child_exited.connect(()=> {
            n.remove_page(n.page_num(scrollbar));
        });
        n.switch_page.connect((page,num)=>{
            var TabTitle=((Vte.Terminal)((scrolled)page).vte).get_window_title();
            if (TabTitle.length<5){
                TabTitle="sterm";
            }
            w.title=TabTitle;
        });
        button.set_focus_on_click(false);
        button.add(image);
        var hbox = new HBox(false, 2);
        hbox.pack_start(label, false, false ,0);
        hbox.pack_start(button, false, false ,0);
        hbox.show_all();
        scrollbar.add(vte);
        n.append_page (scrollbar, hbox);
        n.set_tab_reorderable(scrollbar,true);
        return hbox;
    }
    
    public Gtk.Button addNew(){
        var b = new Gtk.Button();
        var image = new Gtk.Image.from_icon_name ("tab-new-symbolic", Gtk.IconSize.MENU);
        b.add(image);
        b.set_focus_on_click(false);
        b.relief = ReliefStyle.NONE;
        b.clicked.connect(()=>{
            create_notebook_child_label("sterm");
        });
        b.show_all();
        return b;
    }

    public Gtk.Button addSshNew(){
        var b = new Gtk.Button();
        var image = new Gtk.Image.from_icon_name ("network-server-symbolic", Gtk.IconSize.MENU);
        b.add(image);
        b.set_focus_on_click(false);
        b.relief = ReliefStyle.NONE;
        b.clicked.connect(()=>{
            create_notebook_child_label("sterm");
            spawn(getCurrentVTE(arguments),"/usr/bin/sterm-connect");
        });
        b.show_all();
        return b;
    }
    public Gtk.Button addNewWin(){
        var b = new Gtk.Button();
        var image = new Gtk.Image.from_icon_name ("window-new-symbolic", Gtk.IconSize.MENU);
        b.add(image);
        b.set_focus_on_click(false);
        b.relief = ReliefStyle.NONE;
        var cmd="";
        for(int i=0;i<arguments.length;i++){
        cmd+=arguments[i]+" ";
        }
        b.clicked.connect(()=>{
            sterm s = new sterm();
            s.sterm_init(arguments,sterm_name);
        });
        b.show_all();
        return b;
    }
    public Gtk.Menu Hmenu(){
        var Hmenu=new Gtk.Menu();
        var menuzoom100 = new Gtk.ImageMenuItem.with_label("Zoom %100");
        menuzoom100.set_always_show_image(true);
        menuzoom100.set_image(new Gtk.Image.from_icon_name ("zoom-original-symbolic", Gtk.IconSize.MENU));
        menuzoom100.activate.connect(()=>{
            getCurrentVTE(arguments).set_font_scale(1.1);
        });
        Hmenu.append(menuzoom100);
        
        var menuzoomIN = new Gtk.ImageMenuItem.with_label("Zoom in");
        menuzoomIN.set_always_show_image(true);
        menuzoomIN.set_image(new Gtk.Image.from_icon_name ("zoom-in-symbolic", Gtk.IconSize.MENU));
        menuzoomIN.activate.connect(()=>{
            getCurrentVTE(arguments).set_font_scale(getCurrentVTE(arguments).get_font_scale()+0.1);
        });
        Hmenu.append(menuzoomIN);
        
        var menuzoomOUT = new Gtk.ImageMenuItem.with_label("Zoom out");
        menuzoomOUT.set_always_show_image(true);
        menuzoomOUT.set_image(new Gtk.Image.from_icon_name ("zoom-out-symbolic", Gtk.IconSize.MENU));
        menuzoomOUT.activate.connect(()=>{
            getCurrentVTE(arguments).set_font_scale(getCurrentVTE(arguments).get_font_scale()-0.1);
        });
        Hmenu.append(menuzoomOUT);
        
        var menureset = new Gtk.ImageMenuItem();
        menureset.set_always_show_image(true);
        menureset.set_image(new Gtk.Image.from_icon_name ("view-refresh-symbolic", Gtk.IconSize.MENU));
        menureset.set_label("Reset");
        menureset.activate.connect(()=>{
        getCurrentVTE(arguments).reset(false,true);
        var command = GLib.Environment.get_variable ("SHELL");
            spawn(getCurrentVTE(arguments),command);    
        });
        Hmenu.append(menureset);
        
        Wmenu.show_all();
        Hmenu.show_all();
        return Hmenu;
    }
}
/*
string string_random(int length = 8, string charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890"){
      string random = "";
    
      for(int i=0;i<length;i++){
            int random_index = Random.int_range(0,charset.length);
            string ch = charset.get_char(charset.index_of_nth_char(random_index)).to_string();
            random += ch;
      }
    
      return random;
}*/
