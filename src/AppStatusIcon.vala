using Gtk;

class AppStatusIcon : Window {
	private StatusIcon trayicon;
	private Gtk.Menu menuSystem;
	private string[] args;
	private string WindowTitle;
	public AppStatusIcon(string[] a,string TooltipText) {
		/* Create tray icon */
		args=a;
		WindowTitle=TooltipText;
		trayicon = new StatusIcon.from_icon_name ("sterm");;
		trayicon.set_tooltip_text (TooltipText);
		trayicon.set_visible(true);
		
		trayicon.activate.connect(new_sterm);
		
		create_menuSystem();
		trayicon.popup_menu.connect(menuSystem_popup);
	}
	
	/* Create menu for right button */
	public void create_menuSystem() {
		menuSystem = new Gtk.Menu();
		var menuNew = new ImageMenuItem();
		menuNew.set_image (new Gtk.Image.from_icon_name ("window-new-symbolic", Gtk.IconSize.MENU));
		menuNew.set_label("New Window");
		menuNew.activate.connect(new_sterm);
		menuSystem.append(menuNew);
		var menusshNew = new ImageMenuItem();
		menusshNew.set_image (new Gtk.Image.from_icon_name ("network-server-symbolic", Gtk.IconSize.MENU));
		menusshNew.set_label("New Ssh connection");
		menusshNew.activate.connect(new_ssh_sterm);
		menuSystem.append(menusshNew);
		var menuQuit = new ImageMenuItem();
		menuQuit.set_image (new Gtk.Image.from_icon_name ("application-exit-symbolic", Gtk.IconSize.MENU));
		menuQuit.set_label("Exit");
		menuQuit.activate.connect(Gtk.main_quit);
		menuSystem.append(menuQuit);
		menuSystem.show_all();
	}
	
	/* Show popup menu on right button */
	private void menuSystem_popup(uint button, uint time) {
		menuSystem.popup(null, null, null, button, time);
	}
	
	public void new_sterm() {
		sterm s = new sterm();
		s.sterm_init(args,WindowTitle);
	}
	public void new_ssh_sterm() {
		sterm s = new sterm();
		s.sterm_init(args,WindowTitle);
		s.spawn(s.getCurrentVTE(arguments),"/usr/bin/sterm-connect");
	}
}
