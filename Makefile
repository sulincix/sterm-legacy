SHELL=/bin/bash
DESTDIR=/
VFLAGS=-X -O3 -X -s -X -Wno-all

all: clean build

build:
	mkdir -p build || true
	valac src/main.vala src/wslblock.vala src/AppStatusIcon.vala --disable-since-check --pkg gtk+-3.0 \
	    --enable-deprecated --pkg vte-2.91 --pkg gio-2.0 --pkg posix -o build/sterm $(VFLAGS)
install:
	mkdir -p $(DESTDIR)/usr/bin || true
	mkdir -p $(DESTDIR)/usr/share/applications || true
	mkdir -p $(DESTDIR)/usr/share/icons/hicolor/scalable/apps || true
	mkdir -p $(DESTDIR)/usr/share/glib-2.0/schemas/ || true
	mkdir -p $(DESTDIR)/etc/xdg/autostart/ || true
	install build/sterm $(DESTDIR)/usr/bin/sterm
	cp -prf src/gsettings.xml $(DESTDIR)/usr/share/glib-2.0/schemas/org.sulin.sterm.gschema.xml
	install src/sterm.svg $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/sterm.svg
	install src/sterm.desktop $(DESTDIR)/usr/share/applications/sterm.desktop
	install src/sterm-single $(DESTDIR)/usr/bin/sterm-single
	install ssh/sterm-connect.sh $(DESTDIR)/usr/bin/sterm-connect
	chmod +x $(DESTDIR)/usr/bin/sterm-single
install-tray:
	install src/sterm-tray.desktop $(DESTDIR)/etc/xdg/autostart/
clean:
	rm -rf build || true
glib-compile-schemas:
	glib-compile-schemas $(DESTDIR)/usr/share/glib-2.0/schemas/
